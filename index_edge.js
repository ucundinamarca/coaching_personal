/*jslint */
/*global AdobeEdge: false, window: false, document: false, console:false, alert: false */
(function (compId) {

    "use strict";
    var im='images/',
        aud='media/',
        vid='media/',
        js='js/',
        fonts = {
        },
        opts = {
            'gAudioPreloadPreference': 'auto',
            'gVideoPreloadPreference': 'auto'
        },
        resources = [
        ],
        scripts = [
        ],
        symbols = {
            "stage": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "both",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            id: 'stage1',
                            type: 'group',
                            rect: ['1', '0', '1023', '641', 'auto', 'auto'],
                            c: [
                            {
                                id: 'fondo1',
                                type: 'image',
                                rect: ['0px', '0px', '1023px', '637px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"fondo.png",'0px','0px']
                            },
                            {
                                id: 'image_intro',
                                type: 'image',
                                rect: ['379px', '0px', '644px', '641px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"image_intro.png",'0px','0px']
                            },
                            {
                                id: 'title',
                                type: 'image',
                                rect: ['11px', '32px', '533px', '362px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"title.png",'0px','0px']
                            },
                            {
                                id: 'btn_start',
                                type: 'image',
                                rect: ['161px', '408px', '155px', '156px', 'auto', 'auto'],
                                cursor: 'pointer',
                                fill: ["rgba(0,0,0,0)",im+"btn_start.png",'0px','0px']
                            }]
                        },
                        {
                            id: 'stage2',
                            type: 'group',
                            rect: ['1', '0', '1023', '637', 'auto', 'auto'],
                            c: [
                            {
                                id: 'fondo2',
                                type: 'image',
                                rect: ['0px', '0px', '1023px', '637px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"fondo.png",'0px','0px']
                            },
                            {
                                id: 'container-slider',
                                type: 'group',
                                rect: ['11px', '14', '1000', '615', 'auto', 'auto'],
                                c: [
                                {
                                    id: 'slider',
                                    type: 'group',
                                    rect: ['0px', '0px', '1000', '615', 'auto', 'auto'],
                                    c: [
                                    {
                                        id: 'slide1',
                                        type: 'rect',
                                        rect: ['0px', '0px', '1000px', '615px', 'auto', 'auto'],
                                        fill: ["rgba(255,255,255,0.00)"],
                                        stroke: [0,"rgba(0,0,0,1)","none"],
                                        userClass: "slides",
                                        c: [
                                        {
                                            id: 'icon_consulte',
                                            type: 'image',
                                            rect: ['0px', '454px', '162px', '163px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"icon_consulte.png",'0px','0px']
                                        },
                                        {
                                            id: 't1',
                                            type: 'text',
                                            rect: ['110px', '77px', '780px', '458px', 'auto', 'auto'],
                                            text: "<p style=\"margin: 0px;\">​</p>",
                                            font: ['Arial, Helvetica, sans-serif', [24, ""], "rgba(0,0,0,1)", "normal", "none", "", "break-word", "normal"]
                                        }]
                                    },
                                    {
                                        id: 'slide2',
                                        type: 'rect',
                                        rect: ['0px', '0px', '1000px', '615px', 'auto', 'auto'],
                                        fill: ["rgba(255,255,255,0.00)"],
                                        stroke: [0,"rgba(0,0,0,1)","none"],
                                        userClass: "slides",
                                        c: [
                                        {
                                            id: 'image_coaching',
                                            type: 'image',
                                            rect: ['0px', '82px', '453px', '448px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"image_coaching.png",'0px','0px']
                                        },
                                        {
                                            id: 't2',
                                            type: 'text',
                                            rect: ['461px', '148px', '507px', '391px', 'auto', 'auto'],
                                            text: "<p style=\"margin: 0px;\">​</p>",
                                            font: ['Arial, Helvetica, sans-serif', [24, ""], "rgba(0,0,0,1)", "normal", "none", "", "break-word", "normal"]
                                        },
                                        {
                                            id: 'title_coaching',
                                            type: 'image',
                                            rect: ['461px', '71px', '501px', '222px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"title_coaching.png",'0px','0px']
                                        }]
                                    },
                                    {
                                        id: 'slide3',
                                        type: 'rect',
                                        rect: ['0px', '0px', '1000px', '615px', 'auto', 'auto'],
                                        fill: ["rgba(255,255,255,0.00)"],
                                        stroke: [0,"rgba(0,0,0,1)","none"],
                                        userClass: "slides",
                                        c: [
                                        {
                                            id: 'tabla2',
                                            type: 'image',
                                            rect: ['636px', '120px', '283px', '405px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"tabla2.svg",'0px','0px']
                                        },
                                        {
                                            id: 'tabla1',
                                            type: 'image',
                                            rect: ['99px', '87px', '422px', '501px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"tabla1.svg",'0px','0px']
                                        },
                                        {
                                            id: 'n1',
                                            type: 'image',
                                            rect: ['26px', '134px', '64px', '64px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"n1.svg",'0px','0px']
                                        },
                                        {
                                            id: 'n2',
                                            type: 'image',
                                            rect: ['26px', '221px', '64px', '64px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"n2.svg",'0px','0px']
                                        },
                                        {
                                            id: 'n3',
                                            type: 'image',
                                            rect: ['26px', '308px', '64px', '64px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"n3.svg",'0px','0px']
                                        },
                                        {
                                            id: 'n4',
                                            type: 'image',
                                            rect: ['26px', '394px', '64px', '64px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"n4.svg",'0px','0px']
                                        },
                                        {
                                            id: 'n5',
                                            type: 'image',
                                            rect: ['26px', '486px', '64px', '64px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"n5.svg",'0px','0px']
                                        },
                                        {
                                            id: 'n6',
                                            type: 'image',
                                            rect: ['556px', '157px', '64px', '64px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"n6.svg",'0px','0px']
                                        },
                                        {
                                            id: 'n7',
                                            type: 'image',
                                            rect: ['556px', '245px', '64px', '64px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"n7.svg",'0px','0px']
                                        },
                                        {
                                            id: 'n8',
                                            type: 'image',
                                            rect: ['556px', '335px', '64px', '64px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"n8.svg",'0px','0px']
                                        },
                                        {
                                            id: 'n9',
                                            type: 'image',
                                            rect: ['557px', '422px', '64px', '64px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"n9.svg",'0px','0px']
                                        },
                                        {
                                            id: 'title_tables',
                                            type: 'image',
                                            rect: ['239px', '24px', '577px', '39px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"title_tables.svg",'0px','0px']
                                        }]
                                    },
                                    {
                                        id: 'slide4',
                                        type: 'rect',
                                        rect: ['0px', '0px', '1000px', '615px', 'auto', 'auto'],
                                        fill: ["rgba(255,255,255,0.00)"],
                                        stroke: [0,"rgba(0,0,0,1)","none"],
                                        userClass: "slides",
                                        c: [
                                        {
                                            id: 'icon-mano_5',
                                            type: 'image',
                                            rect: ['528px', '99px', '453px', '448px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"icon-mano_5.png",'0px','0px']
                                        },
                                        {
                                            id: 'title_5',
                                            type: 'image',
                                            rect: ['11px', '63px', '496px', '171px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"title_5.png",'0px','0px']
                                        },
                                        {
                                            id: 't4',
                                            type: 'text',
                                            rect: ['11px', '200px', '490px', '288px', 'auto', 'auto'],
                                            text: "<p style=\"margin: 0px;\">​</p>",
                                            align: "left",
                                            font: ['Arial, Helvetica, sans-serif', [24, "px"], "rgba(0,0,0,1)", "400", "none", "normal", "break-word", "normal"],
                                            textStyle: ["", "", "", "", "none"]
                                        }]
                                    },
                                    {
                                        id: 'slide5',
                                        type: 'rect',
                                        rect: ['0px', '0px', '1000px', '615px', 'auto', 'auto'],
                                        fill: ["rgba(255,255,255,0.00)"],
                                        stroke: [0,"rgba(0,0,0,1)","none"],
                                        userClass: "slides",
                                        c: [
                                        {
                                            id: 'text_drag',
                                            type: 'image',
                                            rect: ['30px', '157px', '606px', '398px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"text_drag.svg",'0px','0px']
                                        },
                                        {
                                            id: 'title_activity',
                                            type: 'image',
                                            rect: ['68px', '14px', '489px', '239px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"title_activity.png",'0px','0px']
                                        },
                                        {
                                            id: 'icon',
                                            type: 'image',
                                            rect: ['657px', '89px', '337px', '333px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"icon.png",'0px','0px']
                                        },
                                        {
                                            id: 'd1',
                                            type: 'image',
                                            rect: ['695px', '478px', '83px', '20px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"d1.svg",'0px','0px'],
                                            userClass: "arratrables"
                                        },
                                        {
                                            id: 'd2',
                                            type: 'image',
                                            rect: ['877px', '426px', '70px', '18px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"d2.svg",'0px','0px'],
                                            userClass: "arratrables"
                                        },
                                        {
                                            id: 'd3',
                                            type: 'image',
                                            rect: ['788px', '513px', '74px', '23px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"d3.svg",'0px','0px'],
                                            userClass: "arratrables"
                                        },
                                        {
                                            id: 'd4',
                                            type: 'image',
                                            rect: ['690px', '561px', '164px', '31px', 'auto', 'auto'],
                                            clip: 'rect(2px 143px 29px 25px)',
                                            fill: ["rgba(0,0,0,0)",im+"d4.svg",'0px','0px'],
                                            userClass: "arratrables"
                                        },
                                        {
                                            id: 'r4',
                                            type: 'rect',
                                            rect: ['184px', '242px', '119px', '20px', 'auto', 'auto'],
                                            fill: ["rgba(255,255,255,0.00)"],
                                            stroke: [0,"rgb(0, 0, 0)","none"],
                                            userClass: "receptores"
                                        },
                                        {
                                            id: 'r2',
                                            type: 'rect',
                                            rect: ['296px', '445px', '119px', '20px', 'auto', 'auto'],
                                            fill: ["rgba(255,255,255,0.00)"],
                                            stroke: [0,"rgb(0, 0, 0)","none"],
                                            userClass: "receptores"
                                        },
                                        {
                                            id: 'r3',
                                            type: 'rect',
                                            rect: ['127px', '416px', '119px', '20px', 'auto', 'auto'],
                                            fill: ["rgba(255,255,255,0.00)"],
                                            stroke: [0,"rgb(0, 0, 0)","none"],
                                            userClass: "receptores"
                                        },
                                        {
                                            id: 'r1',
                                            type: 'rect',
                                            rect: ['249px', '534px', '119px', '20px', 'auto', 'auto'],
                                            fill: ["rgba(255,255,255,0.00)"],
                                            stroke: [0,"rgb(0, 0, 0)","none"],
                                            userClass: "receptores"
                                        }]
                                    },
                                    {
                                        id: 'slide6',
                                        type: 'rect',
                                        rect: ['0px', '0px', '1000px', '615px', 'auto', 'auto'],
                                        fill: ["rgba(255,255,255,0.00)"],
                                        stroke: [0,"rgba(0,0,0,1)","none"],
                                        userClass: "slides",
                                        c: [
                                        {
                                            id: 'icon_video',
                                            type: 'image',
                                            rect: ['11px', '306px', '379px', '321px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"icon_video.png",'0px','0px']
                                        },
                                        {
                                            id: 'text_videos',
                                            type: 'image',
                                            rect: ['34px', '40px', '334px', '117px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"text_videos.png",'0px','0px']
                                        },
                                        {
                                            id: 't6',
                                            type: 'text',
                                            rect: ['434px', '48px', '500px', '488px', 'auto', 'auto'],
                                            text: "<p style=\"margin: 0px;\">​</p>",
                                            align: "left",
                                            font: ['Arial, Helvetica, sans-serif', [24, "px"], "rgba(0,0,0,1)", "400", "none", "normal", "break-word", "normal"],
                                            textStyle: ["", "", "", "", "none"]
                                        }]
                                    },
                                    {
                                        id: 'slide7',
                                        type: 'rect',
                                        rect: ['0px', '0px', '1000px', '615px', 'auto', 'auto'],
                                        fill: ["rgba(255,255,255,0.00)"],
                                        stroke: [0,"rgba(0,0,0,1)","none"],
                                        userClass: "slides",
                                        c: [
                                        {
                                            id: 'title_causas',
                                            type: 'image',
                                            rect: ['78px', '19px', '841px', '325px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"title_causas.png",'0px','0px']
                                        },
                                        {
                                            id: 'b1',
                                            type: 'image',
                                            rect: ['68px', '209px', '44px', '44px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"b1.png",'0px','0px']
                                        },
                                        {
                                            id: 'b2',
                                            type: 'image',
                                            rect: ['68px', '264px', '44px', '44px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"b2.png",'0px','0px']
                                        },
                                        {
                                            id: 'b3',
                                            type: 'image',
                                            rect: ['68px', '317px', '44px', '44px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"b3.png",'0px','0px']
                                        },
                                        {
                                            id: 'b4',
                                            type: 'image',
                                            rect: ['68px', '369px', '44px', '44px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"b4.png",'0px','0px']
                                        },
                                        {
                                            id: 'b5',
                                            type: 'image',
                                            rect: ['68px', '423px', '44px', '44px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"b5.png",'0px','0px']
                                        },
                                        {
                                            id: 'b6',
                                            type: 'image',
                                            rect: ['68px', '476px', '44px', '44px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"b6.png",'0px','0px']
                                        },
                                        {
                                            id: 't7',
                                            type: 'text',
                                            rect: ['136px', '197px', '739px', '355px', 'auto', 'auto'],
                                            text: "<p style=\"margin: 0px;\">​</p>",
                                            align: "left",
                                            font: ['Arial, Helvetica, sans-serif', [24, "px"], "rgba(0,0,0,1)", "400", "none", "normal", "break-word", "normal"],
                                            textStyle: ["", "", "", "", "none"]
                                        }]
                                    },
                                    {
                                        id: 'slide8',
                                        type: 'rect',
                                        rect: ['0px', '0px', '1000px', '615px', 'auto', 'auto'],
                                        fill: ["rgba(255,255,255,0.00)"],
                                        stroke: [0,"rgba(0,0,0,1)","none"],
                                        userClass: "slides",
                                        c: [
                                        {
                                            id: 'avatar',
                                            type: 'image',
                                            rect: ['45px', '43px', '168px', '541px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"avatar.png",'0px','0px']
                                        },
                                        {
                                            id: 't8',
                                            type: 'text',
                                            rect: ['285px', '42px', '614px', '524px', 'auto', 'auto'],
                                            text: "<p style=\"margin: 0px;\">​</p>",
                                            align: "left",
                                            font: ['Arial, Helvetica, sans-serif', [24, "px"], "rgba(0,0,0,1)", "400", "none", "normal", "break-word", "normal"],
                                            textStyle: ["", "", "", "", "none"]
                                        }]
                                    }]
                                }]
                            },
                            {
                                id: 'btn_prev',
                                type: 'image',
                                rect: ['888px', '575px', '47px', '47px', 'auto', 'auto'],
                                cursor: 'pointer',
                                fill: ["rgba(0,0,0,0)",im+"btn_prev.png",'0px','0px']
                            },
                            {
                                id: 'btn_next',
                                type: 'image',
                                rect: ['952px', '575px', '47px', '46px', 'auto', 'auto'],
                                cursor: 'pointer',
                                fill: ["rgba(0,0,0,0)",im+"btn_next.png",'0px','0px']
                            },
                            {
                                id: 'btn_home2',
                                type: 'image',
                                rect: ['953px', '9px', '58px', '73px', 'auto', 'auto'],
                                cursor: 'pointer',
                                fill: ["rgba(0,0,0,0)",im+"btn_home.png",'0px','0px']
                            },
                            {
                                id: 'btn_credit',
                                type: 'image',
                                rect: ['8px', '9px', '58px', '73px', 'auto', 'auto'],
                                cursor: 'pointer',
                                fill: ["rgba(0,0,0,0)",im+"btn_credit.png",'0px','0px']
                            }]
                        },
                        {
                            id: 'credits',
                            type: 'group',
                            rect: ['24', '13', '988', '607', 'auto', 'auto'],
                            c: [
                            {
                                id: 'Rectangle',
                                type: 'rect',
                                rect: ['-30px', '-18px', '1038px', '659px', 'auto', 'auto'],
                                fill: ["rgba(255,255,255,1.00)"],
                                stroke: [0,"rgba(0,0,0,1)","none"]
                            },
                            {
                                id: 'title_credit',
                                type: 'image',
                                rect: ['0px', '21px', '290px', '85px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"title_credit.png",'0px','0px']
                            },
                            {
                                id: 'btn_home',
                                type: 'image',
                                rect: ['930px', '-4px', '58px', '73px', 'auto', 'auto'],
                                cursor: 'pointer',
                                fill: ["rgba(0,0,0,0)",im+"btn_home.png",'0px','0px']
                            },
                            {
                                id: 'icon_credit',
                                type: 'image',
                                rect: ['687px', '183px', '252px', '292px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"icon_credit.png",'0px','0px']
                            },
                            {
                                id: 'Text5',
                                type: 'text',
                                rect: ['208px', '106px', '369px', '501px', 'auto', 'auto'],
                                text: "<p style=\"margin: 0px;\">​</p>",
                                align: "left",
                                font: ['Arial, Helvetica, sans-serif', [24, "px"], "rgba(0,0,0,1)", "400", "none", "normal", "break-word", "normal"],
                                textStyle: ["", "", "", "", "none"]
                            }]
                        },
                        {
                            id: 'feedback',
                            type: 'group',
                            rect: ['0', '0', '1025', '641', 'auto', 'auto'],
                            c: [
                            {
                                id: 'r_fondo',
                                type: 'image',
                                rect: ['0', '0px', '1025px', '641px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"r_fondo.png",'0px','0px']
                            },
                            {
                                id: 'box',
                                type: 'group',
                                rect: ['45', '47', '946', '561', 'auto', 'auto'],
                                c: [
                                {
                                    id: 'r_box',
                                    type: 'image',
                                    rect: ['0px', '0px', '946px', '561px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"r_box.png",'0px','0px']
                                },
                                {
                                    id: 'r-bad',
                                    type: 'image',
                                    rect: ['679px', '207px', '214px', '176px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"r-bad.png",'0px','0px']
                                },
                                {
                                    id: 'r_good',
                                    type: 'image',
                                    rect: ['679px', '207px', '214px', '176px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"r_good.png",'0px','0px']
                                },
                                {
                                    id: 'r',
                                    type: 'image',
                                    rect: ['205px', '44px', '538px', '61px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"r.svg",'0px','0px']
                                },
                                {
                                    id: 'r-text',
                                    type: 'text',
                                    rect: ['58px', '161px', '599px', '300px', 'auto', 'auto'],
                                    text: "<p style=\"margin: 0px;\">​</p>",
                                    font: ['Arial, Helvetica, sans-serif', [24, ""], "rgba(0,0,0,1)", "normal", "none", "", "break-word", "normal"]
                                }]
                            },
                            {
                                id: 'btn_close',
                                type: 'image',
                                rect: ['949px', '34px', '56px', '57px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"btn_close.png",'0px','0px']
                            }]
                        },
                        {
                            id: 'preload',
                            type: 'group',
                            rect: ['0', '0', '1024', '640', 'auto', 'auto'],
                            c: [
                            {
                                id: 'fondo-preload',
                                type: 'rect',
                                rect: ['0px', '0px', '1024px', '640px', 'auto', 'auto'],
                                fill: ["rgba(255,255,255,1)"],
                                stroke: [0,"rgb(0, 0, 0)","none"]
                            },
                            {
                                id: 'logo',
                                type: 'image',
                                rect: ['462px', '270px', '100px', '100px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"logo.svg",'0px','0px']
                            }]
                        }
                    ],
                    style: {
                        '${Stage}': {
                            isStage: true,
                            rect: ['null', 'null', '1024px', '640px', 'auto', 'auto'],
                            overflow: 'hidden',
                            fill: ["rgba(255,255,255,1)"]
                        }
                    }
                },
                timeline: {
                    duration: 0,
                    autoPlay: true,
                    data: [

                    ]
                }
            }
        };

    AdobeEdge.registerCompositionDefn(compId, symbols, fonts, scripts, resources, opts);

    if (!window.edge_authoring_mode) AdobeEdge.getComposition(compId).load("index_edgeActions.js");
})("EDGE-247297");
