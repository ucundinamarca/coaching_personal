var ST = "#Stage_";
var S = "Stage_";
ivo.info({
    title: "Guia General",
    autor: "Edilson Laverde Molina",
    date: "",
    email: "edilsonlaverde_182@hotmail.com",
    icon: 'https://www.ucundinamarca.edu.co/images/iconos/favicon_verde_1.png',
    facebook: "https://www.facebook.com/edilson.laverde.molina"
});
var audios = [{
    url: "sonidos/click.mp3",
    name: "clic"
},{
    url: "sonidos/mal.mp3",
    name: "bad"
},{
    url: "sonidos/bien.mp3",
    name: "good"
}

];
var html = [
    {
        id: ST+ 't1',
        html: `
            <p class="textGrande">Consulte el recurso educativo que se presenta a continuación.</p>
            <p class="textGrande">Haga clic en los puntos de información o links que encuentre a medida que avanza en el recurso.</p>
        `
    },
    {
        id: ST+ 't2',
        html: `
            <p class="text">Busca mejorar diferentes áreas de la vida cotidiana: relaciones personales, carrera profesional, familia, etc. Donde la persona no gestiona de forma eficaz las situaciones que se le presentan. En este caso el coach ayuda y facilita el cambio para que la persona alcance sus metas, que pueden ser desde encontrar un nuevo trabajo, conseguir pareja, dejar de fumar, adelgazar, cambiar o mejorar hábitos , entre otros.</p>
        `
    },
    {
        id: ST+ 't4',
        html: `
            <p class="text b left">Shulman, Robyn</p>
            <p class="text">El artículo de la Revista Forbes, le ayudará a visualizar las 5 formas en que puede ayudar el Coaching personal al cumplimiento de metas individuales y superación personal.</p>
            <p class="text b"><b>Artículo: 5 ways a personal <br>coach can help your succeed</b></p>
        `
    },
    {
        id: ST+ 't6',
        html: `
            <p class="text">
                <h1 class="title">Building your inner coach</h1>
                <div class="b p">Brett Ledbetter - TEDxGatewayArch</div>
                <p class="text">Con el fin de conocer las aportes del Coaching personal, así como las aristas que maneja para el éxito individual.</p>
                <div class="b link"><a target="_blank" href="https://www.youtube.com/watch?v=q7a5TIzOmeQ" >https://www.youtube.com/watch?v=q7a5TIzOmeQ</a></div>
                <h1 class="title">Cambia tu mente, cambia tu vida</h1>
                <div class="b p">Margarita Pasos - TEDxManagua</div>
                <p class="text">El cual habla sobre la importancia de controlar el dialogo interior y como esto puede llevar al éxito o al fracaso.</p>
                <div class="b link"><a target="_blank" href="https://www.youtube.com/watch?v=uhZzB5hid6M">https://www.youtube.com/watch?v=uhZzB5hid6M</a></div>
            </p>
        `
    },{
        id: ST+ 't7',
        html: `
            <p class="text left">
                Cuando se quiere emprender un proyecto empresarial.
            </p>
            <p class="text left">
                Cuando se está ante situaciones de cambio.
            </p>
            <p class="text left">
                Cuando se busca claridad de los objetivos profesionales.
            </p>
            <p class="text left">
                Cuando haga falta motivación.
            </p>
            <p class="text left">
                Cuando se quieren potenciar habilidades o talentos.
            </p>
            <p class="text left">
                Cuando se desea eliminar patrones negativos. 
            </p>

        `
    },{
        id: ST+ 't8',
        html: `
            <p class="textMediano">
            La EAE Business School,
            a través de su infografía relaciona las ventajas del Coaching, sus principios y aquellos tipos existentes, que aportan a maximizar la proyección profesional.
            <br>
            <br>
            <br>
            <a target="_blank" href="https://retos-directivos.eae.es/infografia-coaching-maxima-potencia-para-tu-proyeccion-profesional/">(Infografía): Coaching y Proyección Profesional</a>
            </p>
            

        `
    },{
        id: ST+ 'Text5',
        html: `
        <div class="credit">
            <p>
                <div>Nombre del material</div>
                <div>Coaching</div>
            </p>

            <p>
                <div>Experto Temático</div>
                <div>Ruth Arroyo Tovar, Maria Jose Angulo</div>
            </p>

            <p>
                <div>Diseñador Instruccional</div>
                <div>Viviana Camacho</div>
            </p>

            <p>
                <div>Diseño Gráfico</div>
                <div>Andrés Guillermo Martínez Gamba</div>
            </p>

            <p>
                <div>Programador</div>
                <div>Edilson Laverde Molina</div>
            </p>

            <p>
                <div>Diseñador Multimedia</div>
                <div>Luis Francisco Sierra, Ginés Velásquez</div>
            </p>

            <p>
                <div>Líder de Proyectos</div>
                <div>Viviana Jaramillo</div>
            </p>
            <p>
                <div>Oficina de Educación Virtual y a Distancia</div>
                <div>Universidad de Cundinamarca</div>
                <div>2022</div>
            </p>
        </div>
        `
    }
];
var contador=0;
var bien =0;
function main(sym) {
    var t = null;
    udec = ivo.structure({
        created: function () {
            t = this;
            t.events();
            t.animations();
            t.setHTML();
            t.gameDraggAndDrop();
            //precarga audios//
            ivo.load_audio(audios, onComplete = function () {
                ivo(ST + "preload").hide();
                stage1.play();
            });
        },
        methods: {
            setHTML:function () {
                for (var i = 0; i < html.length; i++) {
                    $(html[i].id).html(html[i].html);
                }
            },
            showAlert(msg){
                ivo(ST+"r-text").html(msg);
                feedback.timeScale(1).play();

            },
            gameDraggAndDrop: function () {
                t = this;
                //.arratrables
                $(".arratrables").draggable({
                    revert: true
                });

                //.receptores
                $(".receptores").droppable({
                    drop: function (event, ui) {
                        console.log(ui.draggable.attr("id"));
                        console.log($(this).attr("id"));
                        var id1 = ui.draggable.attr("id").split(S+"d")[1];
                        var id2 = $(this).attr("id").split(S+"r")[1];
                        $(this).droppable("disable");
                        ui.draggable.draggable("disable");
                        ui.draggable.position({
                            of: $(this),
                            my: "center",
                            at: "center"
                        });
                        ui.draggable.draggable("option", "revert", false);
                        console.log(id1 + " " + id2);
                        if (id1 == id2) {
                            ivo.play("good");
                            contador++;
                            bien += 1;
                        } else {
                            ivo.play("bad");
                            contador++;
                        }
                        if (contador == 4) {
                            if (bien >= 3){
                                t.showAlert("¡Qué bien! Recuerde siempre que el Coaching Personal se enfoca principalmente a mejorar aspectos negativos personales, tener un mejor autoconocimiento de sí mismo, evaluar el contexto actual para poder alcanzar las metas planteadas. El Coach a través del diálogo constructivo busca apoyar y facilitar los cambios que se requieren para aquellos resultados deseados.");
                                
                                ivo(ST+"r_good").show();
                                ivo(ST+"r-bad").hide();

                            }else{
                                t.showAlert("El Coaching Personal se enfoca principalmente a mejorar aspectos negativos personales, tener un mejor autoconocimiento de sí mismo, evaluar el contexto actual para poder alcanzar las metas planteadas. El Coach a través del diálogo constructivo busca apoyar y facilitar los cambios que se requieren para aquellos resultados deseados.");
                                ivo(ST+"r_good").hide();
                                ivo(ST+"r-bad").show();
                            }
                            //generamos la escala de 50 puntos para 4 puntos posibles alamacenados variable bien
                            var puntos = (50 / 4) * bien;
                            Scorm_mx = new MX_SCORM(false);
                            console.log("Nombre: " + Scorm_mx.info_user().name + " Id: " + Scorm_mx.info_user().id + " Nota: " + puntos);
                            Scorm_mx.set_score(puntos);

                        }
                    }
                });
            },
            events: function () {
                var t = this;
                ivo(ST + "btn_start").on("click", function () {
                    ivo.play("clic");
                    stage1.timeScale(5).reverse();
                    stage2.timeScale(1).play();
                })
                .on("mouseover", function () {

                })
                .on("mouseout", function () {
                    
                });
                ivo(ST + "btn_credit").on("click", function () {
                    ivo.play("clic");
                    credits.timeScale(1).play();
                })
                .on("mouseover", function () {

                })
                .on("mouseout", function () {
                    
                }).css("cursor", "pointer");
                ivo(ST + "btn_home").on("click", function () {
                    ivo.play("clic");
                    credits.timeScale(4).reverse();
                })
                .on("mouseover", function () {

                })
                .on("mouseout", function () {
                    
                });
                ivo(ST + "btn_home2").on("click", function () {
                    ivo.play("clic");
                    stage2.timeScale(5).reverse();
                    stage1.timeScale(1).play();
                })
                .on("mouseover", function () {

                })
                .on("mouseout", function () {
                    
                });

                //======================================== Controlador de slides ========================================//
                //configuración slider principal//
                var slider=ivo(ST+"slider").slider({
                    slides:'.slides',
                    btn_next:ST+"btn_next",
                    btn_back:ST+"btn_prev",
                    rules:function(rule){
                        console.log("rules"+rule);
                    },
                    onMove:function(page){
                        console.log("onMove"+page);
                        ivo.play("clic");
                    },
                    onFinish:function(){
                    }
                });
                ivo(ST + "btn_close").on("click", function () {
                    ivo.play("clic");
                    feedback.timeScale(5).reverse();
                })
                .on("mouseover", function () {

                })
                .on("mouseout", function () {
                    
                }).css("cursor", "pointer");
                
            },
            animations: function () {
                stage1 = new TimelineMax({onComplete: function () {
                    $(ST + "btn_start").addClass("animated infinite pulse");
                }});
                stage1.append(TweenMax.from(ST + "stage1", .8, {x: 1300, opacity: 0}), 0);
                stage1.append(TweenMax.from(ST + "image_intro", .8, {x: 1300, opacity: 0}), 0);
                stage1.append(TweenMax.from(ST + "title", .8, {y: -1300, opacity: 0}), 0);
                stage1.append(TweenMax.from(ST + "btn_start", .8, {scale: 0, opacity: 0, rotation:900}), 0);

                //stage1.append(TweenMax.staggerFrom(".example", .4, {x: 100, opacity: 0, scaleY: 6, ease: Elastic.easeOut.config(0.3, 0.4)}, .2), 0);
                stage1.stop();

                stage2 = new TimelineMax();
                stage2.append(TweenMax.from(ST + "stage2", .8, {y: 1300, opacity: 0}), 0);
                stage2.stop();

                credits = new TimelineMax();
                credits.append(TweenMax.from(ST + "credits", .8, {x: 1300, opacity: 0}), 0);
                credits.append(TweenMax.from(ST + "title_credit", .8, {x: 1300, opacity: 0}), -.4);
                credits.append(TweenMax.from(ST + "Text5", .8, {x: 1300, opacity: 0}), -.4);
                credits.append(TweenMax.from(ST + "icon_credit", .8, {x: 1300, opacity: 0}), -.4);
                credits.append(TweenMax.from(ST + "btn_home", .8, {x: 1300,rotation:900, opacity: 0}), -.4);
                credits.stop();

                feedback = new TimelineMax();
                feedback.append(TweenMax.from(ST + "feedback", .8, {x: 1300, opacity: 0}), 0);
                feedback.append(TweenMax.from(ST + "btn_close", .8, {x: 1300,rotation:900, opacity: 0}), -.4);
                feedback.stop();
            }
        }
    });
}